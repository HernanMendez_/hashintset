import java.util.Arrays;

public class HashIntSet {

	private class Node{
		public int data;
		public Node next;

		public Node(int value) {
			data = value;
			next = null;
		}
		public Node(int value, Node next) {
			data = value;
			this.next = next;
		}
	}

	private Node[] elements;
	private int size;
	public HashIntSet() {
		elements = new Node[10];
		size = 0;
	}

	public int hash(int i) {
		return (Math.abs(i) % elements.length);
	}

	public void add(int value) {
		if(!contains(value)) {
			int h = hash(value);
			Node newNode = new Node(value);
			newNode.next = elements[h];
			elements[h] = newNode;
			size++;

		}
	}
	public boolean contains(int value) {
		Node current = elements[hash(value)];
		while(current != null) {
			if(current.data == value) {
				return true;
			}
			current = current.next;
		}
		return false;
	}

	public String toString() {
		String s = "";
		for(Node n:elements) {
			Node current = n;
			while(current != null) {
				s += current.data + " ";
				current = current.next;
			}
		}
		return s;
	}

	public void remove(int value) {
		int h = hash(value);
		if(elements[h] != null && elements[h].data == value) {
			elements[h] = elements[h].next;
			size--;
		}else {
			Node current = elements[h];
			while(current != null && current.next != null) {
				if(current.next.data == value) {
					current.next = current.next.next;
					size--;
					return;
				}
				current = current.next;
			}
		}	
	}

	public void addAllHashIntSet(HashIntSet s2) {
		String[] arr = s2.toString().split(" ", 0);
		for (int i=0; arr.length > i; i++) {
			this.add(Integer.parseInt(arr[i]));
		}
	}

	public boolean equalsHashIntSet(HashIntSet s2) {
		String[] arr = s2.toString().split(" ", 0);
		if(s2.toString().length() == this.toString().length()) { 
		for (int i=0; arr.length > i; i++) {
			if(!this.contains(Integer.parseInt(arr[i]))) {
				return false;
			}
		}
		return true;
		}
		return false;
	}

	public void removeAllHashIntSet(HashIntSet s2) {
		String[] arr = s2.toString().split(" ", 0);
		for (int i=0; arr.length > i; i++) {
			this.remove(Integer.parseInt(arr[i]));
		}
	}
	
	public void retainAllHashIntSet(HashIntSet s2) {
		for(int i = 0; i < elements.length; i++) {
			//front
			while (elements[i] != null && !s2.contains(elements[i].data)) {
				elements[i] = elements[i].next;
		    }
			if(elements[i] != null) {
				Node last = elements[i];
				Node current = last.next;
				while(current != null) {
					if(!s2.contains(current.data)) {
						last.next = last.next.next;
					} else {
						last = current;
					}
					current = current.next;
				}	
			}
		}

	}
	
	public int[] toArrayHashIntSet() {
		int[] arr = new int[this.size];
		int index = 0;
		for(Node n:elements) {
			Node current = n;
			while(current != null) {
				arr[index++] = current.data;
				current = current.next;
			}
		}
		return arr;
	}
	
	public static void main(String[] args) {
	}

}